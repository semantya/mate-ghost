from gestalt.web.helpers import *

KIMINO_THEMES = os.environ.get('DJANGO_THEMES', 'gentelella').split(' ')

TEMPLATE_TAG_BUILTINS = {
    'i18n':               'django.templatetags.i18n',
    'staticfiles':        'django.contrib.staticfiles.templatetags.staticfiles',

    #'sekizai_tags':      '',
    #'bootstrap_toolkit': '',

    'subdomainurls':     'subdomains.templatetags.subdomainurls',
    'backend_utils':     'uchikoma.connector.templatetags.backend_utils',
    'uchikoma':          'uchikoma.connector.templatetags.uchikoma',
}

#*******************************************************************************

TIME_ZONE = 'Africa/Casablanca'
LANGUAGE_CODE = 'en-us'

ADMINS = (
    # ('Your Name', 'your_email@uchikoma.com'),
)

#*******************************************************************************

RQ_SHOW_ADMIN_LINK = True

SUIT_CONFIG = {
    'ADMIN_NAME': 'Uchikoma Suit',
    'MENU': (
        {'label': 'Security', 'icon':'icon-lock', 'models': [
            {'label': 'HoneyPot Attempts',     'model': 'admin_honeypot.loginattempt'},
            {'label': 'Report formats',        'model': 'report_builder.format'},
            {'label': 'Report listing',        'model': 'report_builder.report'},
        ]},
        {'label': 'Authorization', 'icon':'icon-key', 'models': [
            {'label': 'Sites',                 'model': 'sites.site'},
            {'label': 'API keys',              'model': 'tastypie.api_key'},
            {'label': 'Oauth2 Access Tokens',  'model': 'provider.accesstoken'},
            {'label': 'Oauth2 Clients',        'model': 'provider.client'},
            {'label': 'Oauth2 Grants',         'model': 'provider.grant'},
            {'label': 'Oauth2 Refresh Tokens', 'model': 'provider.refreshtoken'},
            {'label': 'Social Associations',   'model': 'social.association'},
            {'label': 'Social Nonces',         'model': 'social.nonce'},
            {'label': 'Social User Auth',      'model': 'social.usersocialauth'},
        ]},
        '-',
        {'label': 'Connector', 'icon':'icon-plug', 'models': [
            {'label': 'Groups',                'model': 'auth.group'},
            {'label': 'Identities',            'model': 'connector.identity'},
            {'label': 'Organizations',         'model': 'connector.organization'},
        ]},
        {'label': 'Console', 'icon':'icon-desktop', 'models': [
            {'label': 'Projects',              'model': 'console.project'},
            {'label': 'Teams',                 'model': 'console.team'},
            {'label': 'Queries',               'model': 'console.query'},
        ]},
        '-',
        'cables',
        '-',
        'semantics',
        'opendata',
        '-',
        {'label': 'Redis-Queue',    'url': '/queue/',   'icon':'icon-cogs'},
        {'label': 'SQL Explorer',   'url': '/sql/',     'icon':'icon-database'},
        '-',
        {'label': 'Mongo Naut',     'url': '/mongo/',   'icon':'icon-server'},
        {'label': 'Report Builder', 'url': '/report/',  'icon':'icon-bug'},
    ),
    'HEADER_DATE_FORMAT': 'l, j. F Y', # Saturday, 16th March 2013
    'HEADER_TIME_FORMAT': 'H:i',       # 18:42
    'CONFIRM_UNSAVED_CHANGES': True,
}

################################################################################

TEMPLATE_EXTRA_PROCESSORS = (
    'social.apps.django_app.context_processors.backends',
    'sekizai.context_processors.sekizai',
)

STATICFILES_DIRS = [
    Reactor.FILES('assets'),
    #Reactor.FILES('cdn'),
]+[
    Reactor.THEMES(skin,'assets')
    for skin in KIMINO_THEMES
]

