#!/usr/bin/env python

import os, sys, time, logging
import glob, json, yaml, requests

from pymongo import MongoClient

from django.core.management.base import BaseCommand, CommandError

from gestalt.web.helpers import Reactor

################################################################################

class Commandline(BaseCommand):
    def add_arguments(self, parser):
        if hasattr(self, 'add_options'):
            self.add_options(parser)

    def rpath(self, *x): return os.path.join(self.target_dir, *x)

    def read(self, *x): return open(self.rpath(*x))

    def shell(self, *cmd):
        wrap_cmd = lambda txt: '"'+txt+'"' if (' ' in txt) else txt

        stmt = ' '.join([ wrap_cmd(txt) for txt in cmd ])

        os.system(stmt)

#*******************************************************************************

class UplinkCommand(Commandline):
    def handle(self, *args, **options):
        self.lst = []

        self.prepare(*args, **kwargs)

        self.loop(*args, **kwargs)

        self.terminate(*args, **kwargs)

#*******************************************************************************

class Synchronizer(Commandline):
    def handle(self, *args, **options):
        self.lst = []

        self.prepare(*args, **kwargs)

        for cycle,ars,kws in self.loop(*args, **kwargs):
            cycle(*ars, **kws)

        self.terminate(*args, **kwargs)

################################################################################

class Mongo_Sync(Synchronizer):
    db      = property(lambda self: self.coll)

    def connect(self, database, target):
        self.conn = MongoClient(target)

        link = urlparse(target)

        self.coll = self.conn[link.path[1:]]

