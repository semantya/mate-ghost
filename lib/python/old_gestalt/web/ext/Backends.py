#-*- coding: utf-8 -*-

from __future__ import unicode_literals

from gestalt.core.helpers import *

#*******************************************************************************

import requests

from pymongo.mongo_client import MongoClient

from SPARQLWrapper import SPARQLWrapper as SparqlWrapper
from SPARQLWrapper import JSON          as SparqlJSON

###################################################################################

@extend_reactor('rq')
class Extension(ReactorExt):
    def initialize(self):
        self._queue = {}
        self._tasks = {}

    queues     = property(lambda self: self._queue)
    tasks      = property(lambda self: self._tasks)

    #***************************************************************************

    def _status_cache(self, alias, link):
        from django.core.cache import caches

        key = 'django-watchman-{}'.format(uuid.uuid4())
        value = 'django-watchman-{}'.format(uuid.uuid4())

        try:
            cache = caches[alias]

            cache.set(key, value)
            cache.get(key)
            cache.delete(key)

            return {
                "state": True,
            }
        except Exception,ex:
            return {
                "state": False,
                "error": str(ex),
            }

    #***************************************************************************

    def _status_queue(self, *keys):
        resp = {}

        for key in keys:
            queue = None

            try:
                if key=='failed':
                    queue = django_rq.get_failed_queue()
                else:
                    queue = django_rq.get_queue(alias)

                resp[key] = {
                    "state": True,
                    "waiting": queue.count(),
                }
            except Exception,ex:
                resp[key] = {
                    "state": False,
                    "error": str(ex),
                }

        return resp

    #***************************************************************************

    def queue(self, alias):
        from gestalt.web.utils import rq_job, rq_queue, rq_scheduler

        return rq_queue(alias)

    #***************************************************************************

    def scheduler(self, alias):
        from gestalt.web.utils import rq_job, rq_queue, rq_scheduler

        return rq_scheduler(alias)

    ############################################################################

    def get_task(self, alias):
        if alias in self._tasks:
            return self._tasks[alias]
        else:
            return None

    #***************************************************************************

    def invoke_task(self, alias, *args, **kwargs):
        handler = self.get_task(alias)

        if callable(handler):
            return handler(*args, **kwargs)
        else:
            return None

    #***************************************************************************

    def delay_task(self, alias, *args, **kwargs):
        handler = self.get_task(alias)

        if callable(handler):
            return handler.delay(*args, **kwargs)
        else:
            return None

    ############################################################################

    def register_task(self, *args, **kwargs):
        def do_apply(handler, alias=None, queue='default'):
            from gestalt.web.utils import rq_job, rq_queue, rq_scheduler

            #alias = alias or '.'.join([handler.__module__,handler.__name__])
            alias = (alias or '').strip().lower() or handler.__name__.lower()

            if queue not in self._queue:
                self._queue[queue] = rq_queue(queue)

            resp = rq_job(queue)(handler)

            if alias not in self._tasks:
                self._tasks[alias] = resp

            return resp

        return lambda hnd: do_apply(hnd, *args, **kwargs)

