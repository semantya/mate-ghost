#-*- coding: utf-8 -*-

from __future__ import unicode_literals

from gestalt.core.helpers import *

#*******************************************************************************

import requests

from pymongo.mongo_client import MongoClient

from SPARQLWrapper import SPARQLWrapper as SparqlWrapper
from SPARQLWrapper import JSON          as SparqlJSON

###################################################################################

@extend_reactor('django')
class Extension(ReactorExt):
    def initialize(self):
        self._app = {}

        for key in ('connector','console','cables','devops','semantics','opendata'):
            self._app[key] = '%s.%s' % (self.reactor.namespace, key)

    apps   = property(lambda self: self._app.iteritems())
    themes = property(lambda self: self.reactor.settings.KIMINO_THEMES)

###################################################################################

from . import ORM

from . import BackOffice
from . import Backends

from . import Routing
from . import UI

