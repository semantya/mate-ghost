from gestalt.web.helpers import *

################################################################################

CLOUD_BACKENDS = [
    'social.backends.launchpad.LaunchpadOpenId',
    'social.backends.yahoo.YahooOpenId',
    #'social.backends.evernote.EvernoteOAuth',
]

#*******************************************************************************

def register_social(*args, **kwargs):
    def do_apply(handler, narrow, provider):
        prefix = 'API_%s_' % narrow.upper()

        context = {}

        for key in os.environ.keys():
            if key.startswith(prefix):
                nrw = key.replace(prefix, '')

                context[nrw.lower()] = os.environ[key]

        if len(context):
            for key,value in handler(context):
                name = 'SOCIAL_AUTH_%s_%s' % (narrow.upper(), key.upper())

                globals()[name] = value

            CLOUD_BACKENDS.append('social.backends.%s.%s' % (narrow.lower(), provider))

    return lambda hnd: do_apply(hnd, *args, **kwargs)

################################################################################

@register_social('google', 'GoogleOAuth2')
def detect_google(cfg):
    yield 'OAUTH2_KEY', cfg.get('key', '491752084902-cnj66fo100i84lmpvanbmpac0mlln528.apps.googleusercontent.com')
    yield 'OAUTH2_SECRET', cfg.get('pki', 'e5S55LkLMOIp20bqcXXb6bkH')

    yield 'OAUTH_SCOPE', [
        'https://www.googleapis.com/auth/drive',
        'https://www.googleapis.com/auth/userinfo.profile'
    ]

    yield 'PLUS_AUTH_EXTRA_ARGUMENTS', {
          'access_type': 'offline'
    }

    'social.backends.salesforce.',
    #'social.backends.amazon.AmazonOAuth2',

#*******************************************************************************

@register_social('yahoo', 'YahooOAuth2')
def detect_yahoo(cfg):
    yield 'OAUTH2_KEY', cfg.get('key', 'dj0yJmk9MjQ4dWZhWW1uY2ZQJmQ9WVdrOWNVcG1NVVV3TkdrbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD04Yw--')
    yield 'OAUTH2_SECRET', cfg.get('pki', '8a0eb9e31588ba692d8a14e8decd8ef27b6e4622')

#*******************************************************************************

@register_social('salesforce', 'SalesforceOAuth2')
def detect_salesforce(cfg):
    yield 'OAUTH2_KEY', cfg.get('key', '3MVG9HxRZv05HarRHq6u76DGnTKwN17Jhn1I2oblDZWl9nV6Rdd648610U01YHQvyq.Dbh.mCt7IVMWKujnWR')
    yield 'OAUTH2_SECRET', cfg.get('pki', '1597564653932518566')

#*******************************************************************************

#@register_social('amazon', 'xxxxxxxx')
#def detect_amazon(cfg):
#    yield 'KEY', cfg.get('key', 'xxxxxxx')
#    yield 'SECRET', cfg.get('pki', 'xxxxxxx')

################################################################################

@register_social('facebook', 'FacebookOAuth2')
def detect_facebook(cfg):
    yield 'KEY', cfg.get('key', '1761961887348597')
    yield 'SECRET', cfg.get('pki', '82e780031dd3b97e21cdf4762bd4b041')

    yield 'SCOPE', [
        'public_profile',
        'user_friends',
        'email',
        'user_about_me',
        'user_actions.books',
        'user_actions.fitness',
        'user_actions.music',
        'user_actions.news',
        'user_actions.video',
        'user_events',
        'user_birthday',
        'user_education_history',
        'user_games_activity',
        'user_hometown',
        'user_likes',
        'user_location',
        'user_managed_groups',
        'user_photos',
        'user_posts',
        'user_relationships',
        'user_relationship_details',
        'user_religion_politics',
        'user_tagged_places',
        'user_videos',
        'user_website',
        'user_work_history',
        'read_insights',
        'read_audience_network_insights',
        'read_page_mailboxes',
        'manage_pages',
        'publish_pages',
        'publish_actions',
        'rsvp_event',
        'pages_show_list',
        'pages_manage_cta',
        'pages_manage_instant_articles',
        'ads_read',
        'ads_management',
        'pages_messaging',
        'pages_messaging_phone_number',
    ]

    yield 'PROFILE_EXTRA_PARAMS', {
        'locale': 'en_EN',
        'fields': 'id, name, email, age_range'
    }

#*******************************************************************************

@register_social('instagram', 'InstagramOAuth2')
def detect_instagram(cfg):
    yield 'KEY', cfg.get('key', 'c5188255944b412f9e15245e60867f14')
    yield 'SECRET', cfg.get('pki', '04497bc33dfd4febbe74710c0eb133f9')

    yield 'AUTH_EXTRA_ARGUMENTS', {
        'scope': 'likes comments relationships follower_list',
    }

#*******************************************************************************

@register_social('twitter', 'TwitterOAuth')
def detect_twitter(cfg):
    yield 'KEY', cfg.get('consumer_key', 'XEi7Wj41d16Jrq3sMvo9ICPpR')
    yield 'SECRET', cfg.get('consumer_pki', 'bCKgj20QqgKJQsyuReEhC0N7TTUSJqozVvEovK2LVndZY8beLk')

    yield 'ACCESS_KEY', cfg.get('access_key', '56348132-qbhpxvdoSMpwsMCBs1Va5hLP1zWw8oAzOCLiMnPCl')
    yield 'ACCESS_SECRET', cfg.get('access_pki', 'woZ3WH4zimZmzwYNwI9QJyQYswtaufwQIiBthCLkIlUUV')

#*******************************************************************************

@register_social('flickr', 'FlickrOAuth')
def detect_flickr(cfg):
    yield 'KEY', cfg.get('key', '9cc43b49d7e8c3cd5b62298e3f1d3705')
    yield 'SECRET', cfg.get('pki', '168e7e0b0eaee487')

    yield 'AUTH_EXTRA_ARGUMENTS', {'perms': 'read'}

#*******************************************************************************

@register_social('foursquare', 'FoursquareOAuth2')
def detect_foursquare(cfg):
    yield 'KEY', cfg.get('key', '0TGDID2PRM3ECPFY0OXZRWUF2X1V4JOYBGOVB0WKPIVFJXQQ')
    yield 'SECRET', cfg.get('pki', 'R4AE4LCXGTCZDBYK5UWMF3I5GWVYHX5I4AVBVD3P3IEFV1WM')

#*******************************************************************************

@register_social('vk', 'VKOAuth2')
def detect_vk(cfg):
    yield 'OAUTH2_KEY', cfg.get('key', '5606484')
    yield 'OAUTH2_SECRET', cfg.get('pki', 'CEJZ4kECXEWf1j3VgaFv')

    #yield 'OAUTH2_SCOPE', []

################################################################################

@register_social('linkedin', 'LinkedinOAuth2')
def detect_linkedin(cfg):
    yield 'OAUTH2_KEY', cfg.get('key', '77z0xgqi6gep1v')
    yield 'OAUTH2_SECRET', cfg.get('pki', 'nsMuSZGWBEDZdoAS')

    #yield 'OAUTH2_SCOPE', ['r_basicprofile', 'r_emailaddress', ...]
    #yield 'OAUTH2_FIELD_SELECTORS', ['email-address', 'headline', 'industry']
    #yield 'OAUTH2_EXTRA_DATA', [
    #    ('id', 'id'),
    #    ('firstName', 'first_name'),
    #    ('lastName', 'last_name'),
    #    ('emailAddress', 'email_address'),
    #    ('headline', 'headline'),
    #    ('industry', 'industry')
    #]

#*******************************************************************************

@register_social('dropbox', 'DropboxOAuth2')
def detect_dropbox(cfg):
    yield 'OAUTH2_KEY', cfg.get('key', 'f01f0r4p12xev8x')
    yield 'OAUTH2_SECRET', cfg.get('pki', 'vr4gjv405sy8mp0')

#*******************************************************************************

@register_social('stackoverflow', 'StackoverflowOAuth2')
def detect_stackoverflow(cfg):
    yield 'KEY',     cfg.get('key', 'cid')
    yield 'SECRET',  cfg.get('pki', 'pki')
    yield 'API_KEY', cfg.get('key', 'key')

#*******************************************************************************

@register_social('coinbase', 'CoinbaseOAuth2')
def detect_coinbase(cfg):
    yield 'KEY', cfg.get('key', '72d4f5d1aa1227c372b22e250007e7fe4a6b7b2fc7b796e3c5dbac781f519c8e')
    yield 'SECRET', cfg.get('pki', 'fc150b9753d1f13af6f20054bdc4bd7a9fee5d550562571ee2a6f2299e80f451')

    yield 'SCOPE', ['balance']

#*******************************************************************************

@register_social('vimeo', 'VimeoOAuth1')
def detect_vimeo(cfg):
    yield 'KEY', cfg.get('key', '3cdd2428b614dfa131ca7af6d9f3cfd920aa59e1')
    yield 'SECRET', cfg.get('pki', 'rCPYydPj3Ow+wMW/xTOhCE7UM5unCN2N4q1s3l2yPDg0bfu0o6dOUtyA1+motX+Ft5DyFPr5WfqpuHiZObEAUQh2GfKvuvjiLL3zZNCbZwkBKa51Qfws3cUjaMAQpTzt')

#*******************************************************************************

@register_social('uber', 'UberOAuth2')
def detect_uber(cfg):
    yield 'KEY', cfg.get('key', 'CY5qKtceL8xyUQTgRnaa5-ozZJuUGIVC')
    yield 'SECRET', cfg.get('pki', 'CY5qKtceL8xyUQTgRnaa5-ozZJuUGIVC')

    yield 'SCOPE', ['profile', 'request']

################################################################################

@register_social('pocket', 'PocketAuth')
def detect_pocket(cfg):
    yield 'KEY', cfg.get('token', '53535-4f7a3307e0656d206f53c817')

#*******************************************************************************

@register_social('soundcloud', 'SoundcloudOAuth2')
def detect_soundcloud(cfg):
    yield 'KEY', cfg.get('key', 'aab863dea38f889f4085048667f62b50')
    yield 'SECRET', cfg.get('pki', '507a4b7edcb6fb3194f4d01c24f207e0')

#*******************************************************************************

@register_social('upwork', 'UpworkOAuth')
def detect_upwork(cfg):
    yield 'KEY', cfg.get('key', '5fafdd408e31f7584f2f64cdcd6d8bdf')
    yield 'SECRET', cfg.get('pki', 'c1d257a2737f48cb')

#*******************************************************************************

@register_social('tumblr', 'TumblrOAuth')
def detect_tumblr(cfg):
    yield 'KEY', cfg.get('key', '4W8IRaN0NaUJjnEoQt4YOWSm4AwYvIkWxcfUVzA4RD8HddNzAU')
    yield 'SECRET', cfg.get('pki', '16RL1CpfEZdZYwXUzOrJxXb4hiMFuA1XMPRm3SqHO2MPXbEXlS')

#*******************************************************************************

#@register_social('spotify', 'SpotifyOAuth2')
#def detect_spotify(cfg):
#    yield 'KEY', cfg.get('key', 'xxxxxx')
#    yield 'SECRET', cfg.get('pki', 'xxxxxx')

#*******************************************************************************

@register_social('reddit', 'RedditOAuth2')
def detect_reddit(cfg):
    yield 'KEY', cfg.get('key', 'jiKcMgxZ2S6Xvg')
    yield 'SECRET', cfg.get('pki', '9o4kH2dM3Blf-sDGwNQjMV78GRA')

    yield 'AUTH_EXTRA_ARGUMENTS', {
        'duration': 'permanent',
    }

#*******************************************************************************

@register_social('lastfm', 'LastFmAuth')
def detect_lastfm(cfg):
    yield 'KEY', cfg.get('key', 'b4e861a03720e9eb6fe16ea8af7b5e41')
    yield 'SECRET', cfg.get('pki', 'e7b16a4dec7bfcab0d2ed19ecb74bdf3')

#*******************************************************************************

@register_social('mixcloud', 'MixcloudOAuth2')
def detect_mixcloud(cfg):
    yield 'KEY', cfg.get('key', 'ze6RFZKqUWdMnnH5Mu')
    yield 'SECRET', cfg.get('pki', 'J4d8PTUWF4JfpBmH656a7G6QLgXc5n34')

    yield 'EXTRA_DATA', [
        ('username', 'username'),
        ('name', 'name'),
        ('pictures', 'pictures'),
        ('url', 'url')
    ]

################################################################################

@register_social('github', 'GithubOAuth2')
def detect_github(cfg):
    yield 'KEY', cfg.get('key', 'd2c35ed24d9b8ae3aa0f')
    yield 'SECRET', cfg.get('pki', 'd2c35ed24d9b8ae3aa0f')

    #yield 'SCOPE', []

#*******************************************************************************

@register_social('bitbucket', 'BitbucketOAuth2')
def detect_bitbucket(cfg):
    yield 'OAUTH2_KEY', cfg.get('key', '38vnfDrwVm2v4mBH8a')
    yield 'OAUTH2_SECRET', cfg.get('pki', 'XJmSMbA3R9JnfnvbwZaUjUsj89HsWPb5')

    yield 'USERNAME_AS_ID', True

#*******************************************************************************

@register_social('trello', 'TrelloOAuth')
def detect_trello(cfg):
    yield 'KEY', cfg.get('key', 'c65e3f78706e63f4efe83a12a9329ad5')
    yield 'SECRET', cfg.get('pki', '9623e3ab782c0a615ecbf838ccf369cfafc7f5f326ca40df764277082dfc1c01')

    yield 'APP_NAME', 'Uchikoma Connect'
    yield 'EXPIRATION', '30days'

#*******************************************************************************

@register_social('twilio', 'TwilioAuth')
def detect_twilio(cfg):
    yield 'KEY', cfg.get('key', 'ACd11a5e5d4e783959171ce5022bb80346')
    yield 'SECRET', cfg.get('pki', '1b338e6c8a6bde77ef5a58a77dedb865')

#*******************************************************************************

@register_social('slack', 'SlackOAuth2')
def detect_slack(cfg):
    yield 'KEY', cfg.get('key', '4932747637.73508214134')
    yield 'SECRET', cfg.get('pki', '9f0079152e56cf28338b7e8cda3fec79')

    #yield 'SCOPE', ['identity.basic']
    yield 'SCOPE', [
        '%s:%s' % (group,item)
        for group,listing in [
            ('chat:write',    ['bot','user']),
            ('files',         ['read','write:user']),

            ('groups',        ['read','write','history']),
            ('channels',      ['read','write','history']),
            ('im',            ['read','write','history']),
            ('mpim',          ['read','write','history']),

            ('search',        ['read']),
            ('team',          ['read']),

            ('dnd',           ['read','write']),
            ('pins',          ['read','write']),
            ('stars',         ['read','write']),
            ('reactions',     ['read','write']),
            ('reminders',     ['read','write']),

            ('usergroups',    ['read','write']),
            ('users',         ['read','write']),
            ('users.profile', ['read','write']),
        ]
        for item in listing
    ]

#*******************************************************************************

#@register_social('docker', 'DockerOAuth2')
#def detect_docker(cfg):
#    yield 'KEY', cfg.get('key', 'xxxxxx')
#    yield 'SECRET', cfg.get('pki', 'xxxxxx')

#*******************************************************************************

#SLACK_TOKEN  = os.environ.get('API_SLACK_TOKEN',  '')

HEROKU_TOKEN = os.environ.get('API_HEROKU_TOKEN', '641155c1-fc64-46d3-b80d-8dd99423583b')

################################################################################

CLOUD_BACKENDS = tuple(CLOUD_BACKENDS)

