from gestalt.web.utils import *

from social.pipeline.partial import partial

from gestalt.web.helpers import Reactor

from django.core.management import call_command

################################################################################

from django.core.exceptions import MiddlewareNotUsed

class Startup(object):
    def __init__(self):
        from django.template.base import add_to_builtins

        #import django.template.loader

        from django.conf import settings

        for tags in settings.TEMPLATE_TAG_BUILTINS.values():
            add_to_builtins(tags)

        if len([x for x in os.listdir(Reactor.CORPORA('nltk')) if x!='.gitkeep'])==0:
            import nltk

            for corp in os.environ.get('NLTK_CORPUS', 'book').split(' '):
                nltk.download(corp)

            #call_command('setup', '--nltk')

        raise MiddlewareNotUsed('Startup complete')

################################################################################

@partial
def require_email(strategy, details, user=None, is_new=False, *args, **kwargs):
    if kwargs.get('ajax') or user and user.email:
        return
    elif is_new and not details.get('email'):
        email = strategy.request_data().get('email')
        if email:
            details['email'] = email
        else:
            return redirect('require_email')

################################################################################

def refresh_profile(backend, user, response, *args, **kwargs):
    from uchikoma.connector import tasks

    if True:
        tasks.refresh_identity(user.pk, mail=True, feed=True)
    else:
        tasks.refresh_identity(user.pk, backend.name, mail=True, feed=True)

################################################################################

from django.core.mail import send_mail
from django.core.urlresolvers import reverse

def send_validation(strategy, backend, code):
    from django.conf import settings

    url = '{0}?verification_code={1}'.format(
        reverse('social:complete', args=(backend.name,)),
        code.code
    )
    url = strategy.request.build_absolute_uri(url)
    send_mail('Validate your account', 'Validate your account {0}'.format(url),
              settings.EMAIL_FROM, [code.email], fail_silently=False)

################################################################################

def Enrich(request):
    from django.conf import settings

    from social.backends.utils import load_backends
    from social.backends.google import GooglePlusAuth

    resp = {
        'plus_id': getattr(settings, 'SOCIAL_AUTH_GOOGLE_PLUS_KEY', None),
        'plus_scope': ' '.join(GooglePlusAuth.DEFAULT_SCOPE),
        'available_backends': load_backends(settings.AUTHENTICATION_BACKENDS),
        'site': Reactor.get_site_info(),
    }

    return resp
