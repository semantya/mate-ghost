import os, sys, re, uuid

from datetime import datetime

from urlparse import urlparse

import json, yaml

from dateutil.parser import parse as dt_parser

import pprint

import operator, inspect

from functools import wraps

#*******************************************************************************

def func_decorator(callback):
    return lambda *args, **kwargs: lambda handler: callback(handler, *args, **kwargs)
    
def singleton(*args, **kwargs):
    return lambda handler: handler(*args, **kwargs)

################################################################################

import rdflib

from SPARQLWrapper import SPARQLWrapper as SparqlWrapper
from SPARQLWrapper import JSON          as SparqlJSON

#*******************************************************************************

from bulbs import neo4jserver as Neo4jServ
from bulbs import model       as Neo4Model
from bulbs import property    as Neo4Prop
from bulbs import utils       as Neo4Util

