#!/usr/bin/python

from twisted.internet.defer import inlineCallbacks
from twisted.logger import Logger

from autobahn.twisted.util import sleep
from autobahn.twisted.wamp import ApplicationSession
from autobahn.wamp.exception import ApplicationError

################################################################################

def pubsub(*args, **kwargs):
    def do_apply(handler, channel):
        if not hasattr(handler, '_pubsub'):
            setattr(handler, '_pubsub', [])

        if channel not in handler._pubsub:
            handler._pubsub.append(channel)

        return handler

    return lambda hnd: do_apply(hnd, *args, **kwargs)

#*******************************************************************************

def socket_rpc(*args, **kwargs):
    def do_apply(handler, address):
        if not hasattr(handler, '_rpc'):
            setattr(handler, '_rpc', [])

        if address not in handler._rpc:
            handler._rpc.append(address)

        return handler

    return lambda hnd: do_apply(hnd, *args, **kwargs)

################################################################################

class Registry(object):
    def __init__(self, parent, alias, *args, **kwargs):
        self._prn = parent
        self._key = alias

        self._hnd = {}
        self._lst = {}

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize(*args, **kwargs)

    parent  = property(lambda self: self._prn)
    alias   = property(lambda self: self._key)

    factory = property(lambda self: self._hnd)
    listing = property(lambda self: self._lst)

    log     = property(lambda self: self.parent.log)

    #***************************************************************************

    namespace = property(lambda self: '.'.join([x for x in (
        self.parent.namespace, self.prefix, self.alias
    ) if x]))

    def rpath(self, *parts): return '.'.join([self.namespace]+[x for x in parts])

    #***************************************************************************

    @socket_rpc('create')
    def create(self, provider, narrow, config):
        if narrow not in self.listing:
            if provider in self.factory:
                hnd = self.factory[provider]

                obj = hnd(self, narrow, config)

                self.listing[narrow] = obj

        return self.listing.get(narrow, None)

    @socket_rpc('read')
    def read(self, narrow):
        return self.listing.get(narrow, None)

    @socket_rpc('update')
    def update(self, narrow, config):
        if narrow in self.listing:
            self.listing[narrow].update(config)

    @socket_rpc('delete')
    def delete(self, narrow):
        if narrow in self.listing:
            del self.listing[narrow]

################################################################################

from twisted.internet.defer import inlineCallbacks
from autobahn.twisted.wamp import ApplicationSession, ApplicationRunner

################################################################################

class Implant(ApplicationSession):
    def trigger(self, method, *args, **kwargs):
        callback = getattr(self, method, None)

        if callable(callback):
            return callback(*args, **kwargs)
        else:
            return None

    #***************************************************************************

    def print_result(self, method, res, ex):
        if ex:
            print("call result: {}".format(res))
        else:
            print("call result: {}".format(res))

    ############################################################################

    def route(self, nrw):
        return nrw

    #***************************************************************************


    ############################################################################

    def invoke(self): return lambda hnd: self.call(hnd, *args, **kwargs)
    def topic(self):  return lambda hnd: self.subscription(hnd, *args, **kwargs)

    #***************************************************************************

    def call(self, callback, method, *args, **kwargs):
        target = self.route(method)

        try:
            res = yield self.call(unicode(target), *args, **kwargs)

            callback(target, res, None)
        except Exception as e:
            print("could not call procedure '{0}' : {1}".format(target, ex))

    #***************************************************************************

    def register(self, callback, method):
        target = self.route(method)

        try:
            yield self.register(callback, unicode(target))

            print("procedure '%s' registered." % target)
        except Exception as ex:
            print("could not register procedure '{0}' : {1}".format(target, ex))

    #***************************************************************************

    def subscription(self, callback, narrow):
        target = self.route(narrow)

        try:
            yield self.subscribe(callback, unicode(target))

            print("subscribed to topic '%s'." % target)
        except Exception as ex:
            print("could not subscribe to topic {0} : {1}".format(target, ex))

    ############################################################################

    @inlineCallbacks
    def onJoin(self, details):
        self.trigger('prepare')

        self.trigger('begin')

        counter = 0
        while True:
            self.trigger('before')

            self.trigger('process')

            self.trigger('after')

        self.trigger('finish')

    ############################################################################

    @classmethod
    def commandline(self, hnd):
        pass

