require_os_specs () {
  export TARGET_KEY=$1
  export TARGET_DIR=$RONIN_OS_DIR/$TARGET_KEY

  if [[ ! -d $TARGET_DIR ]] ; then
      git clone https://bitbucket.org/it-issal/$TARGET_KEY.git $TARGET_DIR
  fi

  if [[ ! -d $TARGET_DIR ]] ; then
      echo "Could'nt find the specs for your Operating System '"$RONIN_OS"' at : "$RONIN_SPECS

      echo "Sorry, the Shelter can't bootup. Exiting ..."

      exit
  fi
}

ensure_os_specs () {
  if [[ -f /etc/os-release ]] ; then
      source /etc/os-release

      for KEY in ID ID_LIKE NAME PRETTY_NAME VERSION VERSION_ID HOME_URL SUPPORT_URL BUG_REPORT_URL ANSI_COLOR ; do
        VALUE=`eval 'echo \$'$KEY`

        if [[ "$VALUE" != "" ]] ; then
          eval 'export RONIN_SPECS_'$KEY'="'$VALUE'"'
          eval 'export DISTRIB_'$KEY'="'$VALUE'"'

          #eval "export RONIN_SPECS_"$KEY"=\$"$KEY
          #eval "export DISTRIB_"$KEY"=\$"$KEY

          unset $KEY
        fi
      done
  fi

  #*******************************************************************************************************************************

  case $DISTRIB_ID in
      raspbian)
          export RONIN_OS="raspbian"
          ;;
      debian|Debian)
          export RONIN_OS="debian"
          ;;
      ubuntu|Ubuntu)
          export RONIN_OS="ubuntu"
          ;;
      *)
          case $OSTYPE in
              cygwin)
                  export RONIN_OS="windows"

                  export RONIN_HOME_ROOT=$HOME
                  export RONIN_HOME_DIR=$HOMEDRIVE"\Users"
                  ;;
              *)
                  export RONIN_OS="linux"

                  if [[ -f /usr/bin/yum ]] ; then
                      export RONIN_OS="redhat"
                  fi

                  if [[ -d /Users ]] ; then
                      export RONIN_OS="macosx"

                      export RONIN_HOME_ROOT=/var/root
                      export RONIN_HOME_DIR=/Users
                  fi
                  ;;
          esac
      ;;
  esac

  #*******************************************************************************************************************************

  export RONIN_SPECS=$RONIN_OS_DIR/$RONIN_OS

  require_os_specs $RONIN_OS

  cat $NOH_ROOT/motd.pre
  echo

  #*******************************************************************************

  export OS_NAME=$RONIN_OS
  export OS_PATH=$RONIN_SPECS

  for key in ID NAME VERSION ANSI_COLOR ; do
    if [[ "`eval 'echo \$RONIN_SPECS_'$key`" != "" ]] ; then
      eval "export OS_$key=\$RONIN_SPECS_$key"
    fi
  done

  source $RONIN_SPECS/env.sh

  unset OS_NAME OS_PATH

  for key in ID NAME VERSION ANSI_COLOR ; do
    if [[ "`eval 'echo \$OS_'$key`" != "" ]] ; then
      eval "unset OS_"$key
    fi
  done
}
