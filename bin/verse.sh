#!/bin/bash

export UCHIKOMA_ROOT=~/devel/cognito/ghost

export GHOST_DOMAIN=uchikoma.xyz
export GHOST_PERSON=proto

export RUNNER=python

if [[ -d $UCHIKOMA_ROOT/var/pyenv ]] ; then
    source $UCHIKOMA_ROOT/var/pyenv/bin/activate

    export RUNNER=$UCHIKOMA_ROOT/var/pyenv/bin/python
fi

export TARGET_LINKs=`$RUNNER $UCHIKOMA_ROOT/boot/strap list -f link`

for link in $TARGET_LINKs ; do
    export UCHIKOMA_LINK=$link

    export `$UCHIKOMA_ROOT/bin/urlize $link`

    gnome-terminal -e "$RUNNER $UCHIKOMA_ROOT/boot/strap shell -i bash" -t "Uchikoma :: $link" &
done
