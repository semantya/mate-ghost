#!/usr/bin/python

from python2use.shortcuts import *
from deming.shortcuts import *

################################################################################

class CrossDirective(PersistentDirective):
    def configure(self, *args, **kwargs):
        self.parser.add_argument('--etc', dest='config', action='store_true',
                            default=False, help="Upgrade the System configration (/etc).")

        self.parser.add_argument('--specs', dest='homeless', action='store_true',
                            default=False, help="Upgrade the Homeless specs.")

        self.parser.add_argument('--system', dest='system', action='store_true',
                            default=False, help="Upgrade the Operating system.")

        self.parser.add_argument('--stack', dest='shelter', action='store_true',
                            default=False, help="Upgrade the Shelter stack.")

        self.parser.add_argument('--charm', dest='charms', action='store_true',
                            default=False, help="Status all installed charms.")

        self.parser.add_argument('--all', dest='all', action='store_true',
                            default=False, help="Enable all options above.")

        if hasattr(self, 'reconfigure'):
            if callable(self.reconfigure):
                self.reconfigure(*args, **kwargs)

    MAPPING = [
        ('os',      '/ron/boot/os',    lambda x: 'https://bitbucket.org/maher-os/%s.git'    % x),
        ('charm',   '/ron/usr/charms', lambda x: 'https://bitbucket.org/maher-charm/%s.git' % x),
        ('sdk',     '/ron/usr/sdk',    lambda x: 'https://bitbucket.org/maher-sdk/%s.git'   % x),

        #('jitsu',   '/ron/usr/jitsu',      lambda x: 'https://bitbucket.org/maher-jitsu/%s.git' % x),
        #('recipes', '/ron/usr/recipes',    lambda x: 'https://bitbucket.org/%s/recipes.git'     % x),
    ]

    def populate_repos(self, **opts):
        if opts['config'] or opts['all']:
            yield 1, dict(path='/etc', title="EtcKeeper")

        if opts['homeless'] or opts['all']:
            yield 1, dict(path='/ron/usr', title="Homeless")

        if opts['system'] or opts['all']:
            yield 1, dict(path='/ron/boot/os/%(RONIN_OS)s' % os.environ, title="EtcKeeper")

        if opts['shelter'] or opts['all']:
            yield 1, dict(path='/ron', title="Shelter")

        if opts['charms'] or opts['all']:
            for key in Nucleon.local.lsdir('/ron/usr/charms'):
                yield 2, dict(prefix='->', path='/ron/usr/charms/'+key, title="IT Charm :: %s" % key)

################################################################################

class GitDirective(PersistentDirective):
    def configure(self):
        self.parser.add_argument('-pl', '--pull', dest='pull', action='store_true',
                            help="Pull before.")

        self.parser.add_argument('-ps', '--push', dest='push', action='store_true',
                            help="Push after.")

        self.parser.add_argument('-d', '--default', dest='default_remote', action='store_true',
                            help="Include default remote in operations.")

        self.parser.add_argument('-r', '--remote', dest='remotes', type=str, action='append',
                            default=['origin'], help="Remote to use.")

        if hasattr(self, 'extend'):
            if callable(self.extend):
                self.extend()

        if sys.argv[1] not in ['global']:
            self._pth = os.curdir

            while not Nucleon.local.exists(self.rpath('.git')):
                if self._pth=='/':
                    print "No Git repository in this path. try again ..."

                    exit()
                else:
                    self._pth = os.path.dirname(self._pth)

            self._pth = os.path.abspath(self._pth)

            Nucleon.local.chdir(self.repo_home)

            self.repo = Repository(self.repo_home)

    #************************************************************************************

    def rpath(self, *pth):
        return self.rpath(*pth)

    #************************************************************************************

    repo_home = property(lambda self: self._pth)

    def rpath(self, *pth):
        return os.path.abspath(os.path.join(self.repo_home, *pth))

    @property
    def context(self):
        return {
            'home':  self.repo_home,
        }

    #************************************************************************************

    def do_pull(self, *args, **kwargs):
        #for rmt in self.repo.remotes:
        #    if True or rmt in self.args['remotes']:
        for rmt in self.args['remotes']:
                self.repo.pull(self.args['branch'], rmt)

    def do_push(self, *args, **kwargs):
        #for rmt in self.repo.remotes:
        #    if rmt in self.args['remotes']:
        for rmt in self.args['remotes']:
            self.repo.push(self.args['branch'], rmt)

################################################################################

class DemingCycler(PersistentCommandline):
    TITLE = 'Deming CI Manager.'
    CFG_PATH = '/ron/usr/etc/charms/deming.yml'

    def default_args(self, parser):
        pass

    #************************************************************************************

    def load_config(self, *args, **kwargs):
        if 'author' in self.cfg:
            Nucleon.local.shell('git', 'config', '--global',  '--replace-all', 'user.name',  self.cfg['author']['name'])
            Nucleon.local.shell('git', 'config', '--global',  '--replace-all', 'user.email', self.cfg['author']['email'])

        if 'ops' in self.cfg:
            for key,cfg in self.cfg['ops'].iteritems():
                from demantia.shortcuts import ExtManager

                #inst = ExtManager.invoke(key, cfg)

                #if inst is not None:
                #    pass

    #************************************************************************************

    def before_command(self, *args, **kwargs):
        if hasattr(self, 'repo'):
            kwargs['remotes'] = kwargs.get('remotes', None) or self.repo.remotes

            if kwargs.get('pull', False):
                self.do_pull()

    def after_command(self, *args, **kwargs):
        if hasattr(self, 'repo'):
            if kwargs.get('push', False):
                print dir(self)

                self.parent.do_push()

    ###################################################################################

    class Init(GitDirective):
        HELP = "Initialize a repository with a Demingfile."

        def extend(self):
            self.parser.add_argument('-R', '--recursive', dest='recursive', action='store_true',
                                help="Recursively enhance repositories with Deming.")

        def execute(self, *args, **opts):
            st = True

            if st==True:
                print "-> Initializing repository at '%s' for Deming C.I :" % self.rpath('.')

                if self.rpath('.').startswith('/ron/usr') or self.rpath('.').startswith('/ron'):
                    if 'devel' not in self.repo.remotes:
                        if 'origin' in self.repo.remotes:
                            link = self.repo.remote_links['origin']

                            for fqdn in ('bitbucket.org', 'github.com'):
                                if link.startswith('https://%s/' % fqdn):
                                    link = link.replace('https://%s/' % fqdn, 'git@%s:' % fqdn)

                            Nucleon.local.shell('git', 'remote', 'add', 'backup', self.repo.remote_links['origin'])

                            Nucleon.local.shell('git', 'remote', 'rm', 'origin')

                            Nucleon.local.shell('git', 'remote', 'add', 'origin', link)

                            Nucleon.local.shell('git', 'pull', '-u', 'origin', 'master')

                            Nucleon.local.shell('git', 'push', '-u', 'origin', 'master')

                if opts['recursive']:
                    Nucleon.local.shell('git', 'submodule', 'foreach', 'deming', 'init', '-R')
            else:
                print "#) A problem occured while initializing repository at '%s' for Deming C.I." % self.rpath('.')

            pass # Nucleon.local.shell('git', 'commit', '-a', '-m', '"%(msg)s"' % self.args)

    ###################################################################################

    class Commit(GitDirective):
        def extend(self):
            self.parser.add_argument('-a', '--add', dest='append', action='store_true',
                                help="Add new files to git index.")

            self.parser.add_argument('-m', '--msg', dest='message', type=str,
                                default='', help="Message for any commit.")

            self.parser.add_argument('-b', '--branch', dest='branch', type=str,
                                default='master', help="Git branch to use.")

        def execute(self, **opts):
            if opts['append']:
                Nucleon.local.shell('git', 'add', '--all')

            if opts['append']:
                Nucleon.local.shell('git', 'commit', '-a', '-m', opts['message'])
            else:
                Nucleon.local.shell('git', 'commit', '-m', opts['message'])

    #************************************************************************************

    class Sync(GitDirective):
        def execute(self, *args, **kwargs):
            self.do_pull()

            self.do_push()

            self.repo.submodules()

    ###################################################################################

    class Settle(GitDirective):
        def execute(self, *args, **kwargs):
            self.do_pull()

            self.repo.submodules()

    #************************************************************************************

    class Env(GitDirective):
        def execute(self, *args, **kwargs):
            flv = self.repo.flavor

            if flv:
                flv.environment()

    ###################################################################################

    class Deploy(GitDirective):
        def execute(self, *args, **kwargs):
            self.do_sync()

            for rmt in self.repo.remotes:
                Nucleon.local.shell('ssh', '%(user)s@%(host)s' % self.args, '--', '"%s"' % ' '.join(['/ron/bin/hooks/ci/git-deploy', rmt, self.args['branch'], self.bpath]))

    ###################################################################################

    class Scan(PersistentDirective):
        def configure(self):
            self.parser.add_argument('-pl', '--pull', dest='pull', action='store_true',
                                help="Pull before.")

            self.parser.add_argument('-ps', '--push', dest='push', action='store_true',
                                help="Push after.")

            self.parser.add_argument('-d', '--default', dest='default_remote', action='store_true',
                                help="Include default remote in operations.")

            self.parser.add_argument('-r', '--remote', dest='remotes', type=str, action='append',
                                default=['origin'], help="Remote to use.")

            self.repos = []

            if hasattr(self, 'extend'):
                if callable(self.extend):
                    self.extend()

        def get_repo(self, provider, owner, name):
            for entry in self.repos:
                if (entry['provider']==provider) and (entry['owner']==owner) and (entry['name']==name):
                    return entry

            return None

        def execute(self, *paths, **kwargs):
            for pth in paths:
                for root,dirs,files in os.walk(pth):
                    for fld in dirs:
                        if fld=='.git':
                            repo = Repository(None, fld)

                            for rmt in repo.remotes_links:
                                uri = urlparse(rmt)

                                nrw = uri.path[1:].split('/', 1)

                                nrw = dict(
                                    provider = uri.hostname,
                                    owner    = nrw[0],
                                    name     = nrw[1],
                                )

                                if self.get_repo(**nrw) is None:
                                    entry.extend({
                                        'paths': [

                                        ],
                                    })

                                    self.repos.append(nrw)


    #************************************************************************************

    class List(GitDirective):
        def execute(self, *args, **kwargs):
            flv = self.repo.flavor

            if flv:
                flv.environment()

    ###################################################################################

    class Strider(GitDirective):
        HELP = "Run a Strider-CD command."

        def extend(self, *args, **kwargs):
            pass

        def execute(self, *args, **opts):
            if Nucleon.local.exists('/ron/usr/charms/deming/opt/Strider'):
                Nucleon.local.chdir('/ron/usr/charms/deming/opt/Strider')

                os.environ['HOST']        = '0.0.0.0'
                os.environ['PORT']        = '3000'

                os.environ['SERVER_NAME'] = ""
                os.environ['DB_URI']      = ""

                os.environ['SMTP_HOST']   = ''
                os.environ['SMTP_PORT']   = ''
                os.environ['SMTP_USER']   = ''
                os.environ['SMTP_PASS']   = ''
                os.environ['SMTP_FROM']   = ''

                os.environ['PLUGIN_GITHUB_APP_ID']     = 'hereComesTheId'
                os.environ['PLUGIN_GITHUB_APP_SECRET'] = 'theSecretFromGithub'

                os.environ['PLUGIN_BITBUCKET']         = '{"hostname": "http://appxample.com", "appKey": "as34yih", "appSecret": "f4j83904"}'

                os.environ['HEROKU_OAUTH_ID']          = 'hereComesTheId'
                os.environ['HEROKU_OAUTH_SECRET']      = 'theSecretFromHeroku'

                if args[0]=='run':
                    Nucleon.local.shell('npm', 'start')
                else:
                    Nucleon.local.shell('node', 'bin/strider', *args)
            else:
                print "Strider-CD is not present. Exiting ..."

    ############################################################################

    class Ide(GitDirective):
        HELP = ""

        def extend(self, *args, **kwargs):
            pass

        def execute(self, *args, **opts):
            if Nucleon.local.exists('/ron/usr/charms/devel/src/Cloud9'):
                Nucleon.local.chdir('/ron/usr/charms/devel/src/Cloud9')

                Nucleon.local.shell('node', 'server.js', '-w', self.rpath('.'))
            else:
                print "Cloud9 is not present. Exiting ..."

    ############################################################################

    class Global(CrossDirective):
        HELP = "Status the following : OS, Shelter stack, Homeless specs"

        def reconfigure(self, *args, **kwargs):
            self.parser.add_argument('-l', '--list', dest='listing', action='store_true',
                                default=False, help="Short listing.")

        def execute(self, *args, **opts):
            for lvl,entry in self.populate_repos(**opts):
                entry['spacer']  = int(lvl) * '\t'
                entry['header']  = entry['spacer'] + entry.get('prefix', '#)')
                entry['spacer'] += '  '

                repo = Repository(entry['path'])

                entry['git']  = 'B' if repo.bare  else ' '
                entry['git'] += 'D' if repo.dirty else ' '
                entry['git'] += 'S' if repo.stale else ' '

                if opts['listing']:
                    print "%(header)s %(path)s\t\t\t[%(git)s]" % entry
                else:
                    print "%(header)s title \t: %(title)s" % entry
                    print "%(spacer)s path \t: %(path)s" % entry
                    print "%(spacer)s git \t: %(git)s" % entry

if __name__=='__main__':
    mgr = DemingCycler()

    mgr(*sys.argv)
