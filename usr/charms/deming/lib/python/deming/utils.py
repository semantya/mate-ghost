#-*- coding: utf-8 -*-

class UtraDict(object):
    def __init__(self, ext, key, initial={}):
        self._ext = ext
        self._key = key
        self._val = initial

    extension = property(lambda self: self._ext)
    name      = property(lambda self: self._key)

    def __getitem__(self, key):
        return self._val.get(key, None)

    def __setitem__(self, key, value):
        old_value = self._val[key]

        self._val[key] = value

        self.extension.trigger('updated_%s' % self.name, old_value, value)

        return value

    @property
    def __dict__(self):
        return self._val

    def __iter__(self):
        return self._val.__iter__()

class GenericExt(object):
    def __init__(self, mgr, key, config={}, creds={}, vault={}):
        self._mgr = mgr
        self._key = key
        self._cbs = {}
        self._sch = {}

        self.config = UtraDict(self, 'config', config)
        self.creds  = UtraDict(self, 'creds', creds)
        self.vault  = UtraDict(self, 'vault', vault)

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize()

    manager    = property(lambda self: self._mgr)
    name       = property(lambda self: self._key)

    ontologies = property(lambda self: self._sch.values())

    def callback(self, *args, **kwargs):
        def do_apply(callback, event):
            if event not in self._cbs:
                self._cbs[event] = []

            self._cbs[event].append(callback)

            return callback

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    def trigger(self, event, *args, **kwargs):
        for hnd in self._cbs.get(event, []):
            if callable(hnd):
                hnd(*args, **kwargs)

class ExtManager(object):
    def __init__(self):
        self._reg = {}
        self._sch = {}

    def register(self, *args, **kwargs):
        def do_apply(hnd, key, *params, **opts):
            if key not in self._reg:
                self._reg[key] = hnd

            return hnd

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    def extend(self, *args, **kwargs):
        def do_apply(hnd, key, onto, *params, **opts):
            if key in self._reg:
                self._sch[key] = {}

            if onto not in self._sch[key]:
                self._sch[key][onto] = hnd

            return hnd

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    def resolve(self, key, default=None):
        if key in self._reg:
            return self._reg[key]
        else:
            return default

    def enrich(self, ext):


        return ext

    def invoke(self, key, cfg, creds, vault):
        prvd = self.resolve(key)

        if prvd:
            resp = prvd(self, cfg, creds, vault)

            return resp
        else:
            return None

ExtManager = ExtManager()

class Ontology(object):
    def __init__(self, mgr, key):
        self._mgr = mgr
        self._key = key
        self._objs = {}

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize()

    manager = property(lambda self: self._mgr)
    name    = property(lambda self: self._key)

    def wrap(self, obj, nrw=None):
        if nrw is None:
            nrw = sef.narrow(obj)

        if nrw is None:
            if nrw not in self._objs:
                self._objs[sch][nrw] = self.wrapper(self, nrw, obj)

        return self._objs.get(nrw, None)

    def all(self):
        for raw in self.populate():
            self._objs[self.narrow(raw)] = self.wrap(raw)

        return self._objs.values()

    def __iter__(self): return self.all().__iter__s

    class Instance(object):
        def __init__(self, onto, nrw, target=None):
            self._onto= onto
            self._nrw = nrw
            self._obj = target

            if hasattr(self, 'initialize'):
                if callable(self.initialize):
                    self.initialize()

        ontology = property(lambda self: self._onto)
        manager  = property(lambda self: self.ontology.manager)

        narrow   = property(lambda self: self._nrw)
        target   = property(lambda self: self._obj)

        def get_field(self, key, default=None):
            if hasattr(self, 'extract'):
                return self.extract(key, default)
            else:
                return getattr(self, target, None)

        def __getitem__(self, key, default=None):
            return self.get_field(key, None)

        def __setitem__(self, key, value):
            if hasattr(self, 'enrich'):
                return self.enrich(key, value)
            else:
                return False
