#-*- coding: utf-8 -*-

from demantia.shortcuts import *

################################################################################

import heroku

@ExtManager.register('heroku')
class Heroku_Voodoo(VoodooExt):
    def initialize(self):
        self._cnx = heroku.from_key(self.config['api_key'])

#*******************************************************************************

@ExtManager.extend('heroku','applications')
class Application(Ontology):
    def initialize(self):
        pass

    def populate(self):
        for app in self.manager._cnx.apps:
            yield app

    def narrow(self, obj):
        print obj, dir(obj)

        return obj

    def extract(self, key, default):
        pass

    def invoke(self, method, *args, **kwargs):
        pass

    def call(self, *args, **kwargs):
        pass

    class Wrapper(Ontology.Instance):
        pass

################################################################################

#import redhat

@ExtManager.register('openshift')
class OpenShift_Voodoo(VoodooExt):
    def initialize(self):
        self._cnx = redhat.OpenShift(self.cfg['api_key'])
