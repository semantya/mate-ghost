>>> from pattern.vector import GA, chngrams
>>> from pattern.en import lexicon
>>> from random import choice
>>> 
>>> def chseq(length=4, chars='abcdefghijklmnopqrstuvwxyz'):
>>>     # Returns a string of random characters. 
>>>     return ''.join(choice(chars) for i in range(length))
>>> 
>>> class Jabberwocky(GA):
>>>     
>>>     def fitness(self, w):
>>>         return sum(0.2 for ch in chngrams(w, 4) if ch in lexicon) + \
>>>                sum(0.1 for ch in chngrams(w, 3) if ch in lexicon)
>>>     
>>>     def combine(self, w1, w2):
>>>         return w1[:len(w1)/2] + w2[len(w2)/2:] # cut-and-splice
>>> 
>>>     def mutate(self, w):
>>>         return w.replace(choice(w), chseq(1), 1)   
>>> 
>>> # Start with 10 strings, each 8 random characters.
>>> candidates = [''.join(chseq(8)) for i in range(10)]
>>> 
>>> ga = Jabberwocky(candidates)
>>> i = 0
>>> while ga.avg < 1.0 and i < 1000:
>>>     ga.update(top=0.5, mutation=0.3)
>>>     i += 1
>>> 
>>> print ga.population
>>> print ga.generation
>>> print ga.avg
