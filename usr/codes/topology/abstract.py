import os, sys, time, logging
import operator,math
import glob, json, yaml

import luigi
import heroku

################################################################################

GRID_ADDONs = [
    'heroku-redis','heroku-postgresql',
    'cleardb','mongolab','graphenedb',
    'cloudamqp','cloudmqtt',
    'newrelic','logentries',
]

cloud = heroku.from_key(os.environ['API_HEROKU_TOKEN'])

#*******************************************************************************

def ensure_app(nrw, **opts):
    print "*) Processing node : %(uid)s" % nrw

    resp = None

    try:
        resp = cloud.apps[nrw]

        print "\t-> Retrieving node : %(uid)s" % nrw
    except:
        pass

    if resp is None:
        resp = setup_app(nrw, **opts)

    return resp

#*******************************************************************************

def setup_app(nrw, config={}, addons=[], **opts):
    #print "\t-> Creating node : %(uid)s" % nrw

    app = cloud.apps.add(nrw)

    for plg in GRID_ADDONs+addons:
        try:
            app.addons.add(plg)

            print "\t\t#> Installed plugin : %s" % plg
        except:
            pass

    return app

################################################################################

class Neurotic(luigi.Task):
    alias  = luigi.Parameter(significant=False)

    def output(self):
        from uchikoma.settings import FILES

        return luigi.LocalTarget(FILES('workdir', '%s.txt' % self.narrow))

    def run(self):
        print "\t-> Creating : " + self.narrow

#*******************************************************************************

class Tachikoma(Neurotic):
    narrow = property(lambda self: '%s-%s' % ('uchikoma', self.alias))

    def requires(self):
        resp = []

        for entry in self.develop():
            resp.append(entry)

        for nrw in self.domains:
            if type(nrw) not in (list, set, frozenset):
                nrw = (nrw, 1)

            for i in range(0, nrw[1]):
                entry = Domain(parent=self, alias='know-%s-%d' % (nrw[0], i+1))

                resp.append(entry)

        return resp

#*******************************************************************************

class Cortex(Neurotic):
    parent = luigi.TaskParameter(significant=False)

    narrow = property(lambda self: '%s-%s' % (self.parent.narrow, self.alias))

    def output(self):
        from uchikoma.settings import FILES

        return luigi.LocalTarget(FILES('workdir', '%s.txt' % self.narrow))

    def run(self):
        print "\t-> Creating : " + self.narrow

        #app = ensure_app(self.narrow)

################################################################################

class Body(Cortex):
    pass

#*******************************************************************************

class Lobe(Cortex):
    pass

#*******************************************************************************

class Domain(Cortex):
    def requires(self):
        from . import cognito

        return [
            cognito.RdfStore(parent=self, alias='facts'),
            cognito.RdfStore(parent=self, alias='rules'),
        ]

    @property
    def config(self):
        if not hasattr(self, '_cfg'):
            pth = '/tayaa/srv/intelligence/%s/mapping.yml'

            if os.path.exists(pth):
                self._cfg = yaml.load(open(pth))

        return getattr(self, '_cfg', None) or {
            'require': [],
        }

#*******************************************************************************

class Cognito(Cortex):
    pass

#*******************************************************************************

class Agent(Cortex):
    pass

