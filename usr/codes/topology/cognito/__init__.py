from uchikoma.connect.flows.abstract import *

################################################################################

class RdfStore(Cognito):
    def requires(self):
        return [
        ]

#*******************************************************************************

class SparqlEP(Cognito):
    def requires(self):
        return [
        ]

#*******************************************************************************

class AIML(Cognito):
    def requires(self):
        return [
        ]

################################################################################

class Sense(Cognito):
    def requires(self):
        return [
        ]

#*******************************************************************************

class Markov(Cognito):
    def requires(self):
        return [
        ]

#*******************************************************************************

class Walker(Cognito):
    def requires(self):
        return [
        ]

################################################################################

class Observer(Cognito):
    def requires(self):
        return [
            Sense(parent=self, alias='source'),
            RdfStore(parent=self, alias='veritas'),
            RdfStore(parent=self, alias='experience'),
        ]

#*******************************************************************************

class Reasoner(Cognito):
    def requires(self):
        return [
            RdfStore(parent=self, alias='source'),
            SparqlEP(parent=self, alias='thinker'),
            Walker(parent=self, alias='explorer'),
            RdfStore(parent=self, alias='target'),
        ]

#*******************************************************************************

class Learner(Cognito):
    def requires(self):
        return [
            RdfStore(parent=self, alias='source'),
            SparqlEP(parent=self, alias='thinker'),
            Walker(parent=self, alias='awe'),
            Walker(parent=self, alias='waw'),
            RdfStore(parent=self, alias='target'),
        ]

################################################################################

class Dialect(Cognito):
    def requires(self):
        return [
            RdfStore(parent=self, alias='source'),
            RdfStore(parent=self, alias='target'),
        ]

#*******************************************************************************

class Glossary(Cognito):
    def requires(self):
        return [
            RdfStore(parent=self, alias='source'),
            RdfStore(parent=self, alias='target'),
        ]

#*******************************************************************************

class Linguist(Cognito):
    def requires(self):
        return [
            Dialect(parent=self, alias='source'),
            Glossary(parent=self, alias='target'),
        ]

