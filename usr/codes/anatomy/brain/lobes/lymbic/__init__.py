from uchikoma.abstract.anatomy.brain import *

################################################################################

class Cortex(Lobe):
    def prepare(self):
        pass

        #nerve = self.require('genjutsu://all@%(SPINE_ORGAN)s/%(SPINE_NERVE)s')

        #store = self.require('memory://%(DATA_DOMAIN)s@genjutsu/%(SPINE_ORGAN)s/%(SPINE_NERVE)s')
        #graph = self.require('memory://%(DATA_DOMAIN)s@%(DATA_VERSE)s/%(DATA_NAMESPACE)s')


    #***************************************************************************

    def before(self):
        self.publish(u'com.myapp.oncounter', self.counter)

    def process(self):
        counter += 1

    def after(self):
        yield sleep(1)

    #***************************************************************************

    subscribe = {
        'lymbic://clock/cardio':     'tick_cardio',
        'lymbic://clock/pneumo':     'tick_pneumo',

        'lymbic://signal/alpha':     'signal_alpha',
        'lymbic://signal/beta':      'signal_beta',
        'lymbic://signal/gamma':     'signal_gamma',
    }

    expose = {
        'lymbic://intuition/pain':   'feel_pain',
        'lymbic://intuition/faith':  'feel_faith',
        'lymbic://intuition/happy':  'feel_happy',
        'lymbic://intuition/belong': 'feel_belong',
    }

    #***************************************************************************

    def tick_cardio(self, *args, **kwargs):
        pass

    def tick_pneumo(self, *args, **kwargs):
        pass

    #***************************************************************************

    def signal_alpha(self, *args, **kwargs):
        pass

    def signal_beta(self, *args, **kwargs):
        pass

    def signal_gamma(self, *args, **kwargs):
        pass

    #***************************************************************************

    def feel_pain(self, *args, **kwargs):
        pass

    def feel_faith(self, *args, **kwargs):
        pass

    def feel_happy(self, *args, **kwargs):
        pass

    def feel_belong(self, *args, **kwargs):
        pass

