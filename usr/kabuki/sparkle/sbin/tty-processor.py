#!/usr/bin/env python

import time, random, collectd, serial

#*********************************************************************************

class HookNotFound(Exception):
    def __init__(self, componenet, hook, args=tuple([]), kwargs={}):
        super(Exception, self).__init__()

        self._comp   = component
        self._name   = hook
        self._args   = args
        self._kwargs = kwargs

    component = property(lambda self: self._comp)
    hook_name = property(lambda self: self._name)
    args      = property(lambda self: self._args)
    kwargs    = property(lambda self: self._kwargs)

#*********************************************************************************

class Manager(object):
    def __init__(self, target, endpoint=('10.0.0.1', 25826)):
        self._rrd   = collectd.Connection(target, endpoint[0], endpoint[1])
        self._reg   = {}
        self._canin = {}

    def register_hook(self, nrw, *args, **kwargs):
        if nrw not in self._canin:
            self._canin[nrw] = TTyWatchDog(self, nrw, *args, **kwargs)

        return self._canin.get(nrw, None)

    def register_hook(self, func):
        if callable(func):
            if func.__name__.startswith('process_'):
                nrw = func.__name__[len('process_'):]

                if len(nrw) and (nrw not in self._reg):
                    self._reg[nrw] = func

        return func

    def invoke_hook(self, hook_name, *args, **kwargs):
        if hook_name in self._reg:
            hnd = self._reg[hook_name]

            return hnd(*args, **kwargs)
        else:
            raise HookNotFound(self, hook_name, args, kwargs)

    def send_data(self, narrow, aspect, **payload):
        instance = getattr(self._rrd, narrow, None)

        if instance:
            instance.record(aspect, **payload)

    def asyncloop(self):
        collectd.start_threads()

        while True:
            for dog in self._canin:
                dog.asyncloop()

#*********************************************************************************

class TTyWatchDog(object):
    def __init__(self, manager, narrow, device, baud=9600, refresh=2.0):
        self._mgr   = manager
        self._nrw   = narrow

        self._dev   = device
        self._baud  = baud
        self._cycle = refresh

        self.reset()

    def reset(self):
        self._tty   = serial.Serial(self._dev, baudrate=self._baud, timeout=self._cycle)
        self._buff  = ""

    def send_data(self, aspect, **payload):
        return self._mgr.send_data(self.narrow, aspect, **payload)

    def asyncloop(self):
        ch = port.read()

        if ch in ('\r','\n'):
            if len(raw):
                if "#" in raw:
                    cmd,params = raw.split('#', 1)

                    params = dict([
                        x.split(':',1)
                        for x in params.split('|')
                    ])

                    try:
                        self.manager.invoke_hook(cmd, **params)
                    except HookNotFound,ex:
                        print "<%s>\t%s" % (ex.hook_name, repr(ex.kwargs))
                else:
                    print "error> Unsupported format : '%s'" % raw

            raw = ""
        else:
            raw += ch


##################################################################################

mgr = Manager('phenix')

#*********************************************************************************

@mgr.register_hook
def process_alarm(dog, **params):
    print "{alarm}\t%s" % repr(params)

#*********************************************************************************

@mgr.register_hook
def process_rfid(dog, block0):
    print "{rfid}\t%s" % block0

#*********************************************************************************

@mgr.register_hook
def process_weather(dog, **params):
    for key in params:
        params[key] = float(params[key])

    print "{weather}\t%s" % repr(params)

    dog.send_data('weather', **params)

#*********************************************************************************

if __name__=='__main__':
    mgr.watch_tty('uno', device='/dev/tty'+'AMA0', baud=9600, refresh=2.0)

    mgr.asyncloop()
