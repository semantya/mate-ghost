#!/bin/sh

case "$RONIN_OS" in
  ubuntu|debian|raspbian)
    export APT_PKGs="$APT_PKGs memcached"
    export APT_PKGs="$APT_PKGs sqlite3"
    export APT_PKGs="$APT_PKGs couchdb mongodb"
    export APT_PKGs="$APT_PKGs `echo python-{mysqldb,memcache,redis,couchdb,pymongo}`"

    export APT_PKGs=$APT_PKGs" 'r-cran-*' `echo r-{base,doc-{html,intro,pdf},gnome,mathlib,non{linear,cran-{hmisc,lindsey}},other-{bio3d,genabel,mott-happy},recommended,revolution-revobase}`"
    #apt-get update
    #apt-get install -y python-software-properties python g++ make build-essential
    #add-apt-repository -y ppa:chris-lea/node.js
    #add-apt-repository -y ppa:chris-lea/zeromq
    #apt-get update
    #apt-get install -y nodejs libzmq3-dev libjansson-dev python-sympy python-jinja2 python-dateutil libgsl0-dev
    ;;
  windows)
    export CYG_PKGs=$CYG_PKGs",memcached"
    export CYG_PKGs=$CYG_PKGs",octave,R"
    ;;
  macosx)
    #	ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
    ;;
esac

#*******************************************************************************

if [[ $ZSH != '' ]] ; then
  ZSH_PLUGINs+=(redis-cli postgres)
fi

#*******************************************************************************

export PYPI_PKGs=$PYPI_PKGs" predictionio"

export NPM_PKGs=$NPM_PKGs" deployd ssm ql.io-engine"

