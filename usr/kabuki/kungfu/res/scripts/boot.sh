#!/bin/bash

PKGs="build-essential curl wget unzip cython openssh-server"
PKGs="$PKGs apt-show-versions"
PKGs="$PKGs apache2 libapache2-mod-php5 php5-curl php5-gd php5-memcache php5-mysql"
PKGs="$PKGs python-dev python-setuptools python-numpy python-scipy python-sympy python-matplotlib python-scapy python-scrapy"
PKGs="$PKGs ruby rubygems1.9.1"
PKGs="$PKGs maven openjdk-7-jre openjdk-7-jdk"

apt-get install -y --force-yes $PKGs

for svc in apache2 ; do
  if [[ -e /etc/init.d/$svc ]] ; then
    update-rc.d -f $svc remove

    service $svc stop
  fi
done

gem install --no-rdoc --no-ri foreman sinatra

curl -sS https://getcomposer.org/installer | php

mv composer.phar /usr/bin/composer
