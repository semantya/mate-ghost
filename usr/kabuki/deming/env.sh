#!/bin/sh

# Deming's Shell Cheatsheet

alias settle='deming settle'
alias commit='deming commit'
alias ship='deming ship'
alias devenv='deming env'
alias giting='deming sync'
alias deploy='deming deploy'

#*****************************************************************************************************

case $SHL_OS in
  debian|ubuntu)
    # Developer-oriented aliases

    alias aspire='httrack -w -c8 -Rck -R5 -n -F "Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:27.0) Gecko/20100101 Firefox/27.0" -%v2 -v "-*p3" -N0 -s15 -n'
    alias openyourmind='git submodule foreach --recursive git pull -u origin master'

     #*****************************************************************************************************
    ;;
esac

